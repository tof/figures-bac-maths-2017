## sujet-exercice-4-figure-1

![](images/sujet-exercice-4-figure-1.png)

## sujet-exercice-4-figure-2

![](images/sujet-exercice-4-figure-2.png)

## sujet-exercice-spe-figure-1

![](images/sujet-exercice-spe-figure-1.png)

## sujet-exercice-spe-figure-2

![](images/sujet-exercice-spe-figure-2.png)

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/fr/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/fr/88x31.png" /></a><br />Cette œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/fr/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 3.0 France</a>.