# Sujet de S en Amérique du nord, juin 2017

## corrige-exercice-1-figure-1

![](images/corrige-exercice-1-figure-1.png)

## sujet-exercice-2-figure-1

![](images/sujet-exercice-2-figure-1.png)

## sujet-exercice-2-figure-2

![](images/sujet-exercice-2-figure-2.png)

## sujet-exercice-2-figure-3

![](images/sujet-exercice-2-figure-3.png)

## corrige-exercice-2-figure-1

![](images/corrige-exercice-2-figure-1.png)

## corrige-exercice-2-figure-2

![](images/corrige-exercice-2-figure-2.png)

## sujet-exercice-4-figure-1

![](images/sujet-exercice-4-figure-1.png)

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/fr/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/fr/88x31.png" /></a><br />Cette œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/fr/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 3.0 France</a>.