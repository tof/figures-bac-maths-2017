# Figures du bac de maths 2017

Ce dépôt contient les figures des sujets et des corrigés des épreuves de mathématiques du baccalauréat 2017.

Pour chaque figure est à votre disposition :

- le code source en TikZ ;
- l'image au format .png.

Le code source LaTeX complet des sujets et des corrigés est disponible sur <a href="http://www.apmep.fr/">le site de l'A.P.M.E.P.</a> avec les figures codées en PSTricks.


<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/fr/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/fr/88x31.png" /></a><br />Cette œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/fr/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 3.0 France</a>.